apiVersion: apps/v1
kind: Deployment
metadata:
  name: indexer
  namespace: {{ .Values.namespace }}
spec:
  selector:
    matchLabels:
      run: indexer
  replicas: 1
  template:
    metadata:
      labels:
        run: indexer
    spec:
      containers:
        - name: indexer
          image: {{ .Values.containerRepository }}/indexer:{{ .Values.dockerVersion }}
          env:
            - name: SERVICE_NAME
              value: device-management-vernemq
            - name: NAMESPACE_NAME
              value: {{ .Values.namespace }}
            - name: MQTT_CA_FILE
              value: "/etc/ssl/vernemq/tls.crt"
            - name: TOPIC_NAME
              value: "$share/indexer/iot/farming" # sharing topic "iot/farming between replicas
            - name: PORT
              value: "8883"
            - name: MQTT_USERNAME_PATH
              value: "/etc/credentials/vernemq/username"
            - name: MQTT_PASSWORD_PATH
              value: "/etc/credentials/vernemq/password"
            - name: ELASTICSEARCH_ENDPOINT
              value: https://data-indexing-elasticsearch-es-http:9200
            - name: ELASTICSEARCH_CA_FILE
              value: "/etc/ssl/elasticsearch/tls.crt"
            - name: ELASTICSEARCH_USER
              value: "elastic"
            - name: ELASTICSEARCH_PASSWORD_PATH
              value: "/etc/credentials/elasticsearch/elastic"
            - name: ES_ALIAS
              value: "iot-farming"
          ports:
          - containerPort: 80
          resources:
            limits:
              cpu: 500m
            requests:
              cpu: 200m
          volumeMounts:
            - name: vernemq-certificates
              mountPath: /etc/ssl/vernemq/tls.crt
              subPath: tls.crt
              readOnly: true
            - name: vernemq-credentials
              mountPath: /etc/credentials/vernemq/
              readOnly: true
            - name: elasticsearch-certificates
              mountPath: /etc/ssl/elasticsearch/tls.crt
              subPath: tls.crt
              readOnly: true
            - name: elasticsearch-credentials
              mountPath: /etc/credentials/elasticsearch/
              readOnly: true
      volumes:
        - name: vernemq-certificates
          secret:
            secretName: vernemq-certificates-secret
        - name: vernemq-credentials
          secret:
            secretName: vernemq-user-indexer
        - name: elasticsearch-certificates
          secret:
            secretName: data-indexing-elasticsearch-es-http-certs-public
        - name: elasticsearch-credentials
          secret:
            secretName: data-indexing-elasticsearch-es-elastic-user

---

apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: indexer
  namespace: {{ .Values.namespace }}
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: indexer
  minReplicas: 1
  maxReplicas: 3
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 50

---

apiVersion: v1
kind: Service
metadata:
  name: indexer
  namespace: {{ .Values.namespace }}
  labels:
    run: indexer
spec:
  ports:
    - port: 80
  selector:
    run: indexer
