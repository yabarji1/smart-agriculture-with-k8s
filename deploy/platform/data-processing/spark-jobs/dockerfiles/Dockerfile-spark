FROM openjdk:8-alpine
WORKDIR /opt

ARG HOST_NAME

ENV SPARK_VERSION="2.4.5"
ENV SPARK_HOME="/opt/spark"
ENV HADOOP_VERSION="2.8.5"
ENV HADOOP_HOME="/opt/hadoop"
ENV HOST_NAME=${HOST_NAME}

# spark - Minio self-signed certificate: https://github.com/minio/cookbook/blob/master/docs/apache-spark-with-minio.md
# JVM which runs Hadoop needs a custom ca certificate
COPY ${HOST_NAME}.crt .
RUN keytool -import -trustcacerts -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit -noprompt -alias tls -file ${HOST_NAME}.crt

#https://issues.apache.org/jira/browse/SPARK-26995
#ln -s /lib /lib64 && \
RUN adduser -D -s /bin/sh spark && \
    ln -s /lib /lib64 && \
    apk add --no-cache bash tini libc6-compat && \
    # spark
    wget https://archive.apache.org/dist/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}-bin-without-hadoop.tgz && \
    tar -xzf spark-${SPARK_VERSION}-bin-without-hadoop.tgz && \
    rm spark-${SPARK_VERSION}-bin-without-hadoop.tgz && \
    mv spark-${SPARK_VERSION}-bin-without-hadoop $SPARK_HOME && \
    # hadoop
    wget https://archive.apache.org/dist/hadoop/common/hadoop-${HADOOP_VERSION}/hadoop-${HADOOP_VERSION}.tar.gz && \
    tar -xzf hadoop-${HADOOP_VERSION}.tar.gz && \
    rm hadoop-${HADOOP_VERSION}.tar.gz && \
    mv hadoop-${HADOOP_VERSION} $HADOOP_HOME
RUN wget -O hadoop-aws-${HADOOP_VERSION}.jar https://search.maven.org/remotecontent?filepath=org/apache/hadoop/hadoop-aws/${HADOOP_VERSION}/hadoop-aws-${HADOOP_VERSION}.jar && \
    wget -O httpclient-4.5.8.jar https://search.maven.org/remotecontent?filepath=org/apache/httpcomponents/httpclient/4.5.8/httpclient-4.5.8.jar && \
    wget -O joda-time-2.9.9.jar https://search.maven.org/remotecontent?filepath=joda-time/joda-time/2.9.9/joda-time-2.9.9.jar && \
    wget -O aws-java-sdk-1.11.534.jar https://search.maven.org/remotecontent?filepath=com/amazonaws/aws-java-sdk/1.11.534/aws-java-sdk-1.11.534.jar && \
    wget -O aws-java-sdk-core-1.11.534.jar https://search.maven.org/remotecontent?filepath=com/amazonaws/aws-java-sdk-core/1.11.534/aws-java-sdk-core-1.11.534.jar && \
    wget -O aws-java-sdk-kms-1.11.534.jar https://search.maven.org/remotecontent?filepath=com/amazonaws/aws-java-sdk-kms/1.11.534/aws-java-sdk-kms-1.11.534.jar && \
    wget -O aws-java-sdk-s3-1.11.534.jar https://search.maven.org/remotecontent?filepath=com/amazonaws/aws-java-sdk-s3/1.11.534/aws-java-sdk-s3-1.11.534.jar && \
    #wget -O slf4j-log4j12-1.7.16.jar https://repo1.maven.org/maven2/org/slf4j/slf4j-log4j12/1.7.16/slf4j-log4j12-1.7.16.jar && \
    #wget -O slf4j-api-1.7.16.jar https://repo1.maven.org/maven2/org/slf4j/slf4j-api/1.7.16/slf4j-api-1.7.16.jar && \
    mv *.jar ${SPARK_HOME}/jars/ && \
    cp ${HADOOP_HOME}/share/hadoop/common/*.jar ${SPARK_HOME}/jars/ && \
    cp ${HADOOP_HOME}/share/hadoop/common/lib/*.jar ${SPARK_HOME}/jars/ && \
    cp ${HADOOP_HOME}/share/hadoop/mapreduce/*.jar ${SPARK_HOME}/jars/ && \
    cp ${HADOOP_HOME}/share/hadoop/mapreduce/lib/*.jar ${SPARK_HOME}/jars/ && \
    cp ${HADOOP_HOME}/share/hadoop/mapreduce/lib/*.jar ${SPARK_HOME}/jars/ && \
    chown -R spark:spark ${SPARK_HOME} && \
    rm -rf ${HADOOP_HOME}


ADD spark-entrypoint.sh /opt/entrypoint.sh
RUN chmod a+x /opt/entrypoint.sh && \
    chown spark:spark /opt/entrypoint.sh
USER spark
WORKDIR ${SPARK_HOME}
ENTRYPOINT ["/opt/entrypoint.sh"]
